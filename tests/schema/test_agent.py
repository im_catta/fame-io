from fameio.source.schema import AgentType

from tests.utils import new_agent, new_attribute


def make_agent_type(attributes: list, products: list) -> AgentType:
    """Returns new AgentType with given attributes and products"""
    return AgentType.from_dict("DummyName", new_agent("DummyName", attributes, products)["DummyName"])


class Test:
    def test_init_no_product_and_attributes(self):
        assert make_agent_type([], [])

    def test_init_agent_no_products(self):
        assert make_agent_type([new_attribute("Dummy", "double", False, False, [])], [])

    def test_init_agent_no_attributes(self):
        assert make_agent_type([], ["Product1", "Product2"])

    def test_get_products_empty(self):
        agent = make_agent_type([], [])
        assert isinstance(agent.products, list)

    def test_get_products_no_list(self):
        # noinspection PyTypeChecker
        agent = make_agent_type([], "NotAList")
        products = agent.products
        assert isinstance(products, list)
        assert "NotAList" in products

    def test_get_products_list(self):
        agent = make_agent_type([], ["A", "B"])
        products = agent.products
        assert isinstance(products, list)
        assert "A" in products and "B" in products

    def test_get_attributes_empty(self):
        agent = make_agent_type([], [])
        assert isinstance(agent.attributes, dict)

    def test_get_attributes(self):
        agent = make_agent_type(
            [
                new_attribute("A", "double", False, False, []),
                new_attribute("B", "double", True, True, []),
            ],
            [],
        )
        attributes = agent.attributes
        assert isinstance(attributes, dict)
        assert "A" in attributes and "B" in attributes
